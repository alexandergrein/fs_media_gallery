<?php
defined('TYPO3') || die('not TYPO3 env');

$pluginConfig = ['nested_list', 'flat_list', 'album_by_config', 'album_by_param', 'random'];
foreach ($pluginConfig as $pluginName) {
    $pluginNameForLabel = $pluginName;
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'fs_media_gallery',
        \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($pluginName),
        'LLL:EXT:fs_media_gallery/Resources/Private/Language/locallang_be.xlf:plugin.' . $pluginNameForLabel . '.title',
        null,
        'Media gallery'
    );

    $contentTypeName = 'fsmediagallery_' . str_replace('_', '', $pluginName);
    $flexformFileName = $pluginNameForLabel;

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        'FILE:EXT:fs_media_gallery/Configuration/FlexForms/flexform_' . $flexformFileName . '.xml',
        $contentTypeName
    );
    $GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$contentTypeName] = 'ext-fsmediagallery-plugin-' . str_replace('_', '-', $pluginNameForLabel);

    // todo:
    // $GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['previewRenderer'] = \MiniFranske\FsMediaGallery\Hooks\PluginPreviewRenderer::class;
    $GLOBALS['TCA']['tt_content']['types'][$contentTypeName]['showitem'] = '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            --palette--;;headers,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.plugin,
            pi_flexform,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
            --palette--;;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ';
}
